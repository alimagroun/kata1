
This Spring Boot application provides customer sign-up, sign-in, and delivery request functionalities. It utilizes access and refresh tokens for authentication and authorization.

### Features

- Customer Authentication: Allows users to sign up and sign in securely.
- Token Generation: Upon sign-up or sign-in, the system generates two tokens: an access token and a refresh token.
- Accessing Protected Endpoints: To access protected endpoints (e.g., creating delivery requests), use the generated access token in Postman's Authorization as a Bearer token.
- Delivery Request Creation: Users can create delivery requests.

### Prerequisites

- Java 21 installed on your system.
- Create a database.
- Update the application.yml file with your database configuration.

### Running the Application

- Clone this repository.
- Navigate to the project directory.
- Build the application using Maven or your preferred build tool.
- Run the application using the following command:
java -jar kata-0.0.1-SNAPSHOT.jar
