package com.magroun.kata.dto;

import java.time.LocalDate;

import com.magroun.kata.model.DeliveryMode;
import com.magroun.kata.model.TimeSlot;

public record CustomerDeliveryDetailsDTO(
        int id,
        LocalDate deliveryDate,
        DeliveryMode deliveryMode,
        TimeSlot timeSlot
) {}

