package com.magroun.kata.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.magroun.kata.dto.CustomerDeliveryDetailsDTO;
import com.magroun.kata.model.CustomerDeliveryDetails;
import com.magroun.kata.service.CustomerDeliveryDetailsService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/delivery")
public class CustomerDeliveryDetailsController {

	private final CustomerDeliveryDetailsService deliveryDetailsService;

	public CustomerDeliveryDetailsController(CustomerDeliveryDetailsService deliveryDetailsService) {
	    this.deliveryDetailsService = deliveryDetailsService;
	}

	@PostMapping
	public ResponseEntity<CustomerDeliveryDetailsDTO> createDeliveryDetails(@Valid @RequestBody CustomerDeliveryDetails deliveryDetails) {
	    CustomerDeliveryDetailsDTO createdDetails = deliveryDetailsService.createCustomerDeliveryDetails(deliveryDetails);
	    return ResponseEntity.ok(createdDetails);
	}

    @GetMapping
    public ResponseEntity<List<CustomerDeliveryDetailsDTO>> getAllCustomerDeliveryDetailsByUser() {
            List<CustomerDeliveryDetailsDTO> deliveryDetails = deliveryDetailsService.getAllCustomerDeliveryDetailsByUser();
            return new ResponseEntity<>(deliveryDetails, HttpStatus.OK);
    }
}


