package com.magroun.kata.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.time.LocalTime;

public class TimeRangeValidator implements ConstraintValidator<ValidTimeRange, LocalTime> {

    private static final LocalTime START_TIME = LocalTime.of(8, 0);
    private static final LocalTime END_TIME = LocalTime.of(16, 0);
    private static final LocalTime SECOND_START_TIME = LocalTime.of(9, 0);
    private static final LocalTime SECOND_END_TIME = LocalTime.of(17, 0);

    private String fieldName;

    @Override
    public void initialize(ValidTimeRange constraintAnnotation) {
        fieldName = constraintAnnotation.field();
    }

    @Override
    public boolean isValid(LocalTime value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }

        if ("startTime".equals(fieldName)) {
            if ((value.isAfter(START_TIME) && value.isBefore(END_TIME)) ||
                value.equals(START_TIME) || value.equals(END_TIME)) {
                return true;
            } else {
                context.buildConstraintViolationWithTemplate("Start time should be between 08:00 and 16:00")
                       .addConstraintViolation()
                       .disableDefaultConstraintViolation();
                return false;
            }
        } else if ("endTime".equals(fieldName)) {
            if ((value.isAfter(SECOND_START_TIME) && value.isBefore(SECOND_END_TIME)) ||
                    value.equals(SECOND_START_TIME) || value.equals(SECOND_END_TIME)) {
                return true;
            } else {
                context.buildConstraintViolationWithTemplate("End time should be between 09:00 and 17:00")
                       .addConstraintViolation()
                       .disableDefaultConstraintViolation();
                return false;
            }
        }

        return false;
    }
}


