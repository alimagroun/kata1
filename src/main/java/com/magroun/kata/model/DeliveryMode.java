package com.magroun.kata.model;

public enum DeliveryMode {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}

