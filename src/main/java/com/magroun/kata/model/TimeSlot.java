package com.magroun.kata.model;

import java.time.LocalTime;

import com.magroun.kata.validation.ValidTimeRange;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;

@Entity
public class TimeSlot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull(message = "Start time must be provided")
    @ValidTimeRange(field = "startTime", message = "the time should be between 08:00 and 16:00")
    private LocalTime startTime;

    @ValidTimeRange(field = "endTime", message = "the time should be between 09:00 and 17:00")
    private LocalTime endTime;
    public TimeSlot() {

    }

    public TimeSlot(LocalTime startTime, LocalTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }
}

