package com.magroun.kata.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magroun.kata.dto.AuthenticationRequest;
import com.magroun.kata.dto.AuthenticationResponse;
import com.magroun.kata.dto.RegisterRequest;
import com.magroun.kata.model.Token;
import com.magroun.kata.model.TokenType;
import com.magroun.kata.model.User;
import com.magroun.kata.repository.TokenRepository;
import com.magroun.kata.repository.UserRepository;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class AuthenticationService {
  private final UserRepository repository;
  private final TokenRepository tokenRepository;
  private final PasswordEncoder passwordEncoder;
  private final JwtService jwtService;
  private final AuthenticationManager authenticationManager;
  
  public AuthenticationService(UserRepository repository,
          TokenRepository tokenRepository,
          PasswordEncoder passwordEncoder,
          JwtService jwtService,
          AuthenticationManager authenticationManager) {
	  this.repository = repository;
	  this.tokenRepository = tokenRepository;
	  this.passwordEncoder = passwordEncoder;
	  this.jwtService = jwtService;
	  this.authenticationManager = authenticationManager;
}
  
  public AuthenticationResponse register(RegisterRequest request) {
	  User user = new User(
			    request.getFirstname(),
			    request.getLastname(),
			    request.getEmail(),
			    passwordEncoder.encode(request.getPassword()),
			    request.getPhoneNumber(),
			    request.getStreetAddress(),
			    request.getCity(),
			    request.getState(),
			    request.getPostalCode(),
			    request.getRole()
			);

    var savedUser = repository.save(user);
    var jwtToken = jwtService.generateToken(user);
    var refreshToken = jwtService.generateRefreshToken(user);
    saveUserToken(savedUser, jwtToken);
    AuthenticationResponse authenticationResponse = new AuthenticationResponse();
    authenticationResponse.setAccessToken(jwtToken);
    authenticationResponse.setRefreshToken(refreshToken);

    return authenticationResponse;
  }

  public AuthenticationResponse authenticate(AuthenticationRequest request) {
    authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            request.getEmail(),
            request.getPassword()
        )
    );
    User user = new User();
    user = repository.findByEmail(request.getEmail())
        .orElseThrow();
    var jwtToken = jwtService.generateToken(user);
    var refreshToken = jwtService.generateRefreshToken(user);
    revokeAllUserTokens(user);
    saveUserToken(user, jwtToken);
    AuthenticationResponse authenticationResponse = new AuthenticationResponse();
    authenticationResponse.setAccessToken(jwtToken);
    authenticationResponse.setRefreshToken(refreshToken);

    return authenticationResponse;
  }

  private void saveUserToken(User user, String jwtToken) {
	  Token token = new Token(jwtToken, TokenType.BEARER, false, false, user);
	  tokenRepository.save(token);

  }

  private void revokeAllUserTokens(User user) {
    var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
    if (validUserTokens.isEmpty())
      return;
    validUserTokens.forEach(token -> {
      token.setExpired(true);
      token.setRevoked(true);
    });
    tokenRepository.saveAll(validUserTokens);
  }

  public void refreshToken(
          HttpServletRequest request,
          HttpServletResponse response
  ) throws IOException {
    final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
    final String refreshToken;
    final String userEmail;
    if (authHeader == null ||!authHeader.startsWith("Bearer ")) {
      return;
    }
    refreshToken = authHeader.substring(7);
    userEmail = jwtService.extractUsername(refreshToken);
    if (userEmail != null) {
      var user = this.repository.findByEmail(userEmail)
              .orElseThrow();
      if (jwtService.isTokenValid(refreshToken, user)) {
        var accessToken = jwtService.generateToken(user);
        revokeAllUserTokens(user);
        saveUserToken(user, accessToken);

        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        authenticationResponse.setAccessToken(accessToken);
        authenticationResponse.setRefreshToken(refreshToken);
        new ObjectMapper().writeValue(response.getOutputStream(), authenticationResponse);
      }
    }
  }

}
