package com.magroun.kata.service;

import java.util.List;

import com.magroun.kata.dto.CustomerDeliveryDetailsDTO;
import com.magroun.kata.model.CustomerDeliveryDetails;

public interface CustomerDeliveryDetailsService {
	CustomerDeliveryDetailsDTO createCustomerDeliveryDetails(CustomerDeliveryDetails deliveryDetails);
	List<CustomerDeliveryDetailsDTO> getAllCustomerDeliveryDetailsByUser();
}

