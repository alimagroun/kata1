package com.magroun.kata.service.implementation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.magroun.kata.dto.CustomerDeliveryDetailsDTO;
import com.magroun.kata.exception.UserNotFoundException;
import com.magroun.kata.exception.AuthenticationException;
import com.magroun.kata.model.CustomerDeliveryDetails;
import com.magroun.kata.model.TimeSlot;
import com.magroun.kata.model.User;
import com.magroun.kata.repository.CustomerDeliveryDetailsRepository;
import com.magroun.kata.repository.TimeSlotRepository;
import com.magroun.kata.repository.UserRepository;
import com.magroun.kata.service.CustomerDeliveryDetailsService;

@Service
public class CustomerDeliveryDetailsServiceImpl implements CustomerDeliveryDetailsService {

    private final CustomerDeliveryDetailsRepository deliveryDetailsRepository;
    private final TimeSlotRepository timeSlotRepository;
    private final UserRepository userRepository;

    public CustomerDeliveryDetailsServiceImpl(CustomerDeliveryDetailsRepository deliveryDetailsRepository, 
                                              TimeSlotRepository timeSlotRepository,
                                              UserRepository userRepository) {
        this.deliveryDetailsRepository = deliveryDetailsRepository;
        this.timeSlotRepository = timeSlotRepository;
        this.userRepository = userRepository;
    }

    @Override
    public CustomerDeliveryDetailsDTO createCustomerDeliveryDetails(CustomerDeliveryDetails deliveryDetails) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            String email = ((UserDetails) authentication.getPrincipal()).getUsername(); 

            Optional<User> optionalUser = userRepository.findByEmail(email);
            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                deliveryDetails.setUser(user);
            } else {
                throw new UserNotFoundException("User not found for email: " + email);
            }
        } else {
            throw new AuthenticationException("Authentication failed");
        }

        TimeSlot savedTimeSlot = timeSlotRepository.save(deliveryDetails.getTimeSlot());
        deliveryDetails.setTimeSlot(savedTimeSlot);

        deliveryDetailsRepository.save(deliveryDetails);

        return convertToDTO(deliveryDetails);
    }

    public CustomerDeliveryDetailsDTO convertToDTO(CustomerDeliveryDetails deliveryDetails) {
        return new CustomerDeliveryDetailsDTO(
            deliveryDetails.getId(),
            deliveryDetails.getDeliveryDate(),
            deliveryDetails.getDeliveryMode(),
            deliveryDetails.getTimeSlot()
        );
        
    }
    
    @Override
    public List<CustomerDeliveryDetailsDTO> getAllCustomerDeliveryDetailsByUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            String email = ((UserDetails) authentication.getPrincipal()).getUsername();

            Optional<User> optionalUser = userRepository.findByEmail(email);
            if (optionalUser.isPresent()) {
                User user = optionalUser.get();

                List<CustomerDeliveryDetails> deliveryDetailsList = deliveryDetailsRepository.findAllByUser(user);
                
                List<CustomerDeliveryDetailsDTO> deliveryDetailsDTOList = deliveryDetailsList.stream()
                        .map(this::convertToDTO)
                        .collect(Collectors.toList());

                return deliveryDetailsDTOList;
            } else {
                throw new UserNotFoundException("User not found for email: " + email);
            }
        } else {
            throw new AuthenticationException("Authentication failed");
        }
    }
}
