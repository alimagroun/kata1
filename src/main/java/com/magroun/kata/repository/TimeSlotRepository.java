package com.magroun.kata.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magroun.kata.model.TimeSlot;

public interface TimeSlotRepository extends JpaRepository<TimeSlot, Long> {

}

