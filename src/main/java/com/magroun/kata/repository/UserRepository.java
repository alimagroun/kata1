package com.magroun.kata.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magroun.kata.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

  Optional<User> findByEmail(String email);

}
